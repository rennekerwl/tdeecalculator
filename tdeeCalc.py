

def convertWeight(weight):
    """This function takes a integer imperial weight as input and outputs an integer metric weight"""
    return weight / 2.2046

def convertHeight(height):
    """This function takes a integer imperial height as input and outputs an integer metric height"""
    return height / 0.3937

def calculateMaleBMR(weight, height, age):
    """This function calculates the BMR for a male.  It takes 3 integers as input, weight in kg, height in cm, and age. It outputs BMR as an integer."""
    return 66.473 + (13.7516 * weight) + (5.0033 * height) - (6.755 * age)

def calculateFemaleBMR(weight, height, age):
    """This function calculates the BMR for a female.  It takes 3 integers as input, weight in kg, height in cm, and age. It outputs BMR as an integer."""
    return 447.593 + (9.247 * weight) + (3.098 * height) - (4.33 * age)

def calculateActivityLevel(daysOfExcercise):
    """This function takes an integer, the number of days/week someone exercises, as input.  It outputs a float, which is used as a multiplier to calculate the TDEE."""
    if daysOfExcercise < 1:
        return 1.2
    elif 1 <= daysOfExcercise <= 3:
        return 1.375
    elif 3 < daysOfExcercise <= 5:
        return 1.55
    elif 6 <= daysOfExcercise <= 7:
        return 1.725
    elif 7 < daysOfExcercise:
        return 1.9

def main():
    while True: 
        sex = input("Are you male or female? ")
        if sex.lower().startswith('f') or sex.lower().startswith('m'):
            break

    weightImp = int(input("What is your weight in pounds? "))
    heightImp = int(input("What is your height in inches? "))
    age = int(input("What is your age?" ))
    activity = int(input("How many days per week do you exercise? "))
    weightMetric = convertWeight(weightImp)
    heightMetric = convertHeight(heightImp)
    activityMult = calculateActivityLevel(activity)

    if sex.lower().startswith('f'):
        BMR = calculateFemaleBMR(weightMetric, heightMetric, age)
        TDEE = BMR * activityMult
    elif sex.lower().startswith('m'):
        BMR = calculateMaleBMR(weightMetric, heightMetric, age)
        TDEE = BMR * activityMult
    print("Your BMR is: " + str(int(BMR)) + " Calories.")
    print("Your TDEE is: " + str(int(TDEE)) + " Calories.")
    

if __name__ == '__main__':
    main()

